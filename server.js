require('dotenv').config()
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const middleware = require('./modules/middleware')
const logger = require('./modules/logger')

function logRequest(req, res, next) {
    logger.info(req.url)
    next()
}
server.use(logRequest)

function logError(err, req, res, next) {
    logger.error(err)
    next()
}
server.use(logError)

if (!(process.env.AUTH_HEADER && process.env.QUERY_AUTH_PARAMETER)) {
    logger.error("Missing AUTH_HEADER or QUERY_AUTH_PARAMETER configuration")
    process.exit()
}

var port = process.env.port || 8000;

server.use(middleware)
server.use(router)
server.listen(port, () => {
  logger.info('JSON Server is running on port ' + port)
})
