require('dotenv').config()
const logger = require('./logger');

function isAuthorized(request) {
  logger.debug("Check user authorization")
  let auth_header = request.header(process.env.AUTH_HEADER)
  let query_auth_parameter = request.query[process.env.QUERY_AUTH_PARAMETER]

  if (!auth_header) {
    logger.error(`Missing header ${process.env.AUTH_HEADER} in request`)
    return false
  }

  if (!query_auth_parameter) {
    logger.error(`Missing ${process.env.QUERY_AUTH_PARAMETER} in querysting`)
    return false
  }

  let parser = process.env.HEADER_PARSER || 'default'
  if (parser === 'openlogin') {
    let user = JSON.parse(Buffer.from(auth_header, 'base64').toString())
    auth_header = user.fiscalnumber
  }

  auth_header = auth_header.replace("TINIT-", "").trim()

  logger.debug(`Check: ${query_auth_parameter.toUpperCase()} and ${auth_header.toUpperCase()}`)
  return query_auth_parameter.toUpperCase() === auth_header.toUpperCase()
}

module.exports = (req, res, next) => {
  if (isAuthorized(req)) { // add your authorization logic here
    next() // continue to JSON Server router
  } else {
    res.sendStatus(401)
  }
}
