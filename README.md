# Secured JSON-Server

A [json-server](https://github.com/typicode/json-server) with a simple middleware
to check if the requester if authorized to read a specific information from the API.


## Local Test

Required: docker, docker-compose, httpie

```
$ git clone https://gitlab.com/opencontent/secure-json-server.git 
$ cd secure-json-server
$ docker-compose up -d
$ http http://localhost:8000/test/?secret=hzFdw4f479r9 mysecret:hzFdw4f479r9 
```


